[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.6950485.svg "DOI")](https://doi.org/10.5281/zenodo.6950485)


# `twodpg`

2D polygon geometry related helper functions. Used for image analysis.

Best used in a virtual environment. Depends on `numpy` and `numba`.
To download and install:

```sh
git clone https://gitlab.com/isonder/twodpg.git
cd twodpg
pip install -e .
```
