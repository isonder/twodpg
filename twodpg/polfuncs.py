"""Functionality for 2D polygons.

The functions `pol_contains` and `points_in_polygon` are not my ideas, but were
taken from a StackOverflow thread:
https://stackoverflow.com/questions/36399381/whats-the-fastest-way-of-checking-if-a-point-is-inside-a-polygon-in-python

A number of different algorithms are discussed there. `pol_contains` is the
is_inside_sm function. --- Thank you!
"""
import numpy as np
from numpy.typing import ArrayLike, NDArray
from typing import TypedDict
import numba
from numba import jit, njit


def pol_lengths(pol: NDArray, closed: bool = False) -> NDArray:
    """Returns length of each segment of polygon `pol`.

    Parameters
    ----------
    pol : NDArray
        Shape (N, 2) coordinates of the polygon.
    closed: bool
        Whether to assume the last vertix is the first.
    """
    if closed:
        diff = pol - np.roll(pol, 1)
    else:
        diff = pol[1:] - pol[:-1]
    return np.sqrt(diff[:, 0] ** 2 + diff[:, 1] ** 2)


def centroid(coords: ArrayLike) -> NDArray:
    """Computes the centroid of a polygon, weighed by the length of segments.
    (The implementation also works for 3D catesian coordinates.)

    Parameters
    ----------
    coords : ArrayLike
        The polygon's coordinates as an Nx2 array.

    Returns
    -------
    ndarray
        The centroid's coordinates.
    """
    coords = np.asarray(coords)
    if np.all(coords[0] == coords[-1]):  # Only work on open polygons,
        coords = coords[:-1]             # otherwise the roll goes nuts.
    cshift = np.roll(coords, 1, axis=0)
    lis = np.sum(np.sqrt((cshift - coords) ** 2), axis=1)
    return 0.5 * np.sum(
        lis * (np.transpose(cshift + coords)), axis=1) / lis.sum()


def split_pol_at_largest_distance(pol: NDArray) -> NDArray:
    """Shift polygon elements so that they start with the vertex that has the
    largest distannce to the last vertex.

    Parameters
    ----------
    pol : ndarray
        A shape (N, 2) array.
    """
    diff = pol - np.roll(pol, 1, axis=0)
    idx = np.argmax(diff[:, 0] ** 2 + diff[:, 1] ** 2)
    return np.roll(pol, pol.shape[0] - idx, axis=0)


def scalar(a, b, axis: int = None):
    """Scalar product of two vectors `a` and `b`.
    """
    return np.sum(a * b, axis=axis)


def circumference(pol: ArrayLike) -> float:
    """Computes the circumference of coordinates given in `pol`. The polygon
    will be closed.

    Parameters
    ----------
    pol : array_like
        Shape (N, 2) array with the coordinates.
    """
    pshift = np.roll(pol, 1, axis=0)
    return np.sqrt(np.sum((pshift - pol) ** 2, axis=1)).sum()


def umin(pol: NDArray, rs: ArrayLike) -> NDArray:
    """Compute the values of polygon connection parameter u, so that the
    distance of a polygon segment to reference becomes minimal. Computes u
    values for all segments.

    Parameters
    ----------
    pol : ndarray
        (N, 2) shape array of polygon's coordinates.
    rs : array_like
        Reference points for which to compute the `u`s

    Returns
    -------
    u values in an array of shape (len(rs), len(pol) - 1).
    """
    ret = np.empty((len(rs), len(pol) - 1))
    diff = pol[1:] - pol[:-1]
    for i, r in enumerate(rs):
        ret[i, :] = (
            (diff[:, 0]) * (r[0] - pol[:-1, 0])
            + (diff[:, 1]) * (r[1] - pol[:-1, 1])
        ) / (diff[:, 0] ** 2 + diff[:, 1] ** 2)
    # Clip values to range [0, 1]. Distances to reference outside the range
    # may be smaller, but these do not refer to points on the polygon.
    ret[ret > 1.] = 1.
    ret[ret < 0.] = 0.
    return ret


def dist_min(pol: NDArray, ref: ArrayLike) -> NDArray:
    """Computes minimum distances of reference point(s) `ref` from polygon `pol`.

    Parameters
    ----------
    pol : ndarray
        The polygon as Nx2 array.
    ref : array_like
        One or more reference points.
    """
    ref = np.asarray(ref)
    if ref.ndim == 1:
        ref = np.asarray([ref])
    umins = umin(pol, ref)
    cmin = np.empty((len(ref), 2, len(pol) - 1))
    cmin[:, 0, :] = pol[:-1, 0] + umins * (pol[1:, 0] - pol[:-1, 0])
    cmin[:, 1, :] = pol[:-1, 1] + umins * (pol[1:, 1] - pol[:-1, 1])
    return np.min(
        np.sqrt((ref[:, 0, None] - cmin[:, 0, :]) ** 2
                + (ref[:, 1, None] - cmin[:, 1, :]) ** 2),
        axis=1)


def dist_avg(pol1: NDArray, pol2: NDArray, nsamp: int = 101) -> float:
    """Compute the average distance of two polygons.

    Parameters
    ----------
    pol1, pol2 : NDArray
        Vertices of the two polygons. `pol1` and `pol2` do not need to have
        the same length.
    nsamp : int
        Number of samples to use for integration.
    """
    li1 = pol_lengths(pol1)
    x = np.linspace(0, 1, nsamp, True)
    ret = 0.
    for i, li in enumerate(li1):
        ref = pol1[i] + x[:, None] * (pol1[i + 1] - pol1[i])
        intgr = dist_min(pol2, ref)
        ret += li * np.trapezoid(intgr, dx=1 / (len(x) - 1))
    return ret / li1.sum()


def avg_dist(pol: NDArray, ref: NDArray,
             start: int = 0, close_pol: bool = True) -> float:
    """Calculate an averaged distance of a polygon to a reference point.

    Parameters:
    -----------
    pol : NDArray
        Shape (N, 2) array of polygon's (x, y) coordinates.
    ref : NDArray
        Reference point coordinates.
    start : int
        start index of the integration.
    close_pol : bool
        Whether to connect first and last points.
    """
    intgr = np.linspace(0, 1, 101, True)
    pol = np.roll(pol, start, axis=0)
    pshift = np.roll(pol, 1, axis=0)
    if not close_pol:
        pol = pol[:-1]
        pshift = pshift[:-1]
    lis = np.sum(np.sqrt((pshift - pol) ** 2), axis=1)
    integrals = np.trapezoid(
        np.sqrt(
            lis[:, None] ** 2 * intgr ** 2
            + 2 * scalar((pshift - pol), (pol - ref), axis=1)[:, None] * intgr
            + np.sum((pol - ref) ** 2, axis=1)[:, None]
        ),
        x=intgr, axis=1
    )
    return np.sum(lis * integrals) / lis.sum()


def image_index(width: int, height: int) -> NDArray:
    """Create a shape (`width` * `height`, 2) array that contains indeces of
    each pixel of an image.
    """
    cv, rv = np.meshgrid(np.arange(height), np.arange(width), indexing='ij')
    return np.column_stack([
        rv.reshape(width * height), cv.reshape(width * height)])


def close_pol_if_open(pol: ArrayLike) -> NDArray:
    """Closes a polygon if it is open.

    Appends the first element to the end of vertices if first and last elements
    are different, otherwise the same array is returned.

    Parameters
    ----------
    pol : ArrayLike
        Polygon vertices to 'close'.

    Returns
    -------
    NDArray
        Returns a copy, in case an element is appended, otherwise the returned
        object is determined by `numpy.asarray`.
    """
    pol = np.asarray(pol)
    if np.all(pol[-1] == pol[0]):
        return pol
    else:
        return np.append(pol, pol[None, 0], axis=0)


@jit(nopython=True)
def pol_contains(pol: NDArray, point: NDArray) -> int:
    """
    Returns
    -------
    int
        0 - the point is outside the polygon
        1 - the point is inside the polygon
        2 - the point is one edge (boundary)
    """
    length = len(pol) - 1
    dy2 = point[1] - pol[0][1]
    intersections = 0
    i, k = 0, 1
    while i < length:
        dy = dy2
        dy2 = point[1] - pol[k][1]
        # consider only lines which are not completely above/bellow/right
        # from the point
        if dy * dy2 <= 0.0 and (point[0] >= pol[i][0]
                                or point[0] >= pol[k][0]):
            # non-horizontal line
            if dy < 0 or dy2 < 0:
                f = dy * (pol[k][0] - pol[i][0]) / (dy - dy2) + pol[i][0]
                # if line is left from the point - the ray moving towards left,
                # will intersect it
                if point[0] > f:
                    intersections += 1
                elif point[0] == f:  # point on boundary
                    return 2
            # point on upper peak (dy2=dx2=0)
            # or horizontal line (dy=dy2=0 and dx*dx2<=0)
            elif dy2 == 0 and (
                    point[0] == pol[k][0]
                    or (dy == 0 and (point[0] - pol[i][0]) * (point[0] - pol[k][0]) <= 0)
            ):
                return 2
        i = k
        k += 1
    return intersections & 1


@njit(parallel=True)
def points_in_polygon_parallel(points: NDArray, pol: NDArray) -> NDArray:
    """
    """
    ln = len(points)
    ret = np.empty(ln, dtype=numba.int32)
    for i in numba.prange(ln):
        ret[i] = pol_contains(pol, points[i])
    return ret


def points_in_polygon(points: ArrayLike, pol: ArrayLike) -> NDArray:
    """For each point test if that lies inside `pol`.

    Parameters
    ----------
    points : ArrayLike
    pol : ArrayLike

    Returns
    -------
    NDArray
        Per-element result of `pol_pol_contains`.
    """
    pol = close_pol_if_open(pol)
    points = np.asarray(points)
    return points_in_polygon_parallel(points, pol)


class CircleProps(TypedDict):
    center: ArrayLike
    radius: float


class EllipseProps(TypedDict):
    center: ArrayLike
    angle: float
    major: float
    minor: float


def points_in_circle(points: NDArray, circle: CircleProps, eps: float = 1e-6) -> NDArray:
    """For each x, y pair given in `points`, determine whether it lies inside,
    outside or on the boundary of `circle`.

    Parameters
    ----------
    points : (Nx2) NDArray
        x,y-values of points to test.
    circle : dict
        Dictionary describing the circle. Must have the keys 'center'
        and 'radius'.
    eps : float, optional
        Differences in squared distances of points from boundary  (relative to
        squared radius) smaller than `eps` are interpreted to be on boundary.

    Returns
    -------
        NDArray
    Returns an integer valued array with meaning:
        1: point lies inside the circle
        2: point lies on ellipse boundary
        3: point lies outside the circle
    """
    ret = -np.ones(len(points), dtype=int)
    diffsq = np.sum((points - circle['center']) ** 2, axis=1)
    rsq = circle['radius'] ** 2
    ret[diffsq < rsq] = 1                    # inside
    ret[diffsq > rsq] = 0                    # outside
    idx = np.abs(diffsq - rsq) <= eps * rsq  # distances at the boundary
    ret[idx] = 2
    return ret


def points_in_ellipse(points: NDArray, ell: EllipseProps, eps: float = 1e-6) -> NDArray:
    """For each x, y pair given in `points`, determine whether it lies inside,
    outside or on the boundary of ellipse `ell`.

    Parameters
    ----------
    points : (Nx2) NDArray
        x,y-values of points to test.
    ell : dict
        Dictionary describing the ellipse. Must have the keys 'center', 'angle',
        'major', 'minor'.
    eps : float, optional
        Differences in squared distances of points from boundary  (relative to
        squared minor axis) smaller than `eps` are interpreted to be on boundary.

    Returns
    -------
        NDArray
    Returns an integer valued array with meaning:
        1: point lies inside ellipse
        2: point lies on ellipse boundary
        3: point lies outside ellipse
    """
    ret = - np.ones(len(points), dtype=int)
    diffs = points - ell['center']
    diffs_sq = np.sum(diffs ** 2, axis=1)
    cosalph, sinalph = np.cos(ell['angle']), np.sin(ell['angle'])
    cosphisq = (diffs[:, 0] * cosalph + diffs[:, 1] * sinalph) ** 2 / diffs_sq
    sinphisq = (diffs[:, 1] * cosalph - diffs[:, 0] * sinalph) ** 2 / diffs_sq
    dellsq = ell['major'] ** 2 * cosphisq + ell['minor'] ** 2 * sinphisq
    ret[dellsq > diffs_sq] = 1
    ret[dellsq < diffs_sq] = 0
    ret[np.abs(dellsq - diffs_sq) < eps * ell['minor'] ** 2] = 2
    return ret
