import unittest
import numpy as np

from twodpg import (
    points_in_polygon, points_in_circle, points_in_ellipse, image_index
)


class PointInPolTestCase(unittest.TestCase):
    """Test case mainly for points_in_polygon(). Creates a circle, some image
    coordinates and runs some checks on it.
    """

    def setUp(self) -> None:
        self.radius = 50.
        self.center = np.array([50, 50])
        phis = np.linspace(0, 2 * np.pi, 30, endpoint=False)
        self.x = self.radius * np.cos(phis) + self.center[1]
        self.y = self.radius * np.sin(phis) + self.center[0]
        self.circle = np.stack((self.y, self.x), axis=1)
        self.iidx = image_index(1920, 1080)

    def test_points_in_polygon(self):
        """Testing points_in_polygon()"""
        coords = self.iidx + .5
        pts = points_in_polygon(coords, self.circle) > 0
        area = np.count_nonzero(pts)
        real_area = self.radius ** 2 * np.pi
        tolerance = .1 * real_area
        print(
            f"real area: {real_area}, tolerance: +-{tolerance}\n"
            f"measured area: {area}"
        )
        self.assertAlmostEqual(area, real_area, delta=tolerance)


class PointsInCase(unittest.TestCase):
    def test_points_in_circle(self):
        """Test if three characteristic points are part of circle."""
        correct = np.array([1, 2, 0])
        circle = dict(radius=1., center=[0, 0])
        points = np.array([[.5, .5], [0., 1.], [1., 1.]])
        result = points_in_circle(points, circle)
        if not np.all(result == correct):
            print(f"{result=}, {correct=}")
        self.assertTrue(np.all(result == correct))

    def test_points_in_ellipse(self):
        """Test if three characteristic points, (.5, .5), (1, 0), (1, 1) are
        included in a horizontal ellipse of major/minor axes 1, 0.5.
        """
        correct = np.array([1, 2, 0])
        ell = dict(major=1., minor=.5, angle=0., center=[0, 0])
        points = np.array([[.5, .5], [1., 0.], [1., 1.]])
        result = points_in_ellipse(points, ell)
        if not np.all(result == correct):
            print(f"{result=}, {correct=}")
        self.assertTrue(np.all(result == correct))

    def test_points_in_ellipse_45(self):
        """Test if three characteristic points are inside a rotated ellipse."""
        correct = np.array([1, 2, 0])
        ell = dict(major=1., minor=.5, angle=np.pi / 4, center=[0, 0])
        points = np.array([[.5, .5], [1 / np.sqrt(2), 1 / np.sqrt(2)], [-1., 1.]])
        result = points_in_ellipse(points, ell)
        if not np.all(result == correct):
            print(f"{result=}, {correct=}")
        self.assertTrue(np.all(result == correct))


if __name__ == '__main__':
    unittest.main()
